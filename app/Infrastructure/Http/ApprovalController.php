<?php

namespace App\Infrastructure\Http;

use App\Domain\Enums\StatusEnum;
use App\Domain\InvoiceService;
use App\Infrastructure\Controller;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Approval\Application\ApprovalFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LogicException;
use Ramsey\Uuid\Uuid;
use Exception;

class ApprovalController extends Controller
{
    /**
     * @param string $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function get(string $id)
    {
        try {
            $invoiceService = new InvoiceService(DB::connection());

            $response = [];
            $response['invoice'] = (array)$invoiceService->getById($id);
            $response['invoice']['items'] = $invoiceService->getItens($id)->toArray();

            return response($response, 200)
                ->header('Content-Type', 'text/json');
        } catch (LogicException $ex) {
            return response(['error' => true, 'msg' => $ex->getMessage()], 400)
                ->header('Content-Type', 'text/json');
        } catch (Exception $ex) {
            return response(['error' => true, 'msg' => $ex->getMessage()], 400)
                ->header('Content-Type', 'text/json');
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function approve(string $id)
    {
        try {
            $invoiceService = new InvoiceService(DB::connection());
            $invoice = $invoiceService->getById($id);

            $approvalDto = new ApprovalDto(Uuid::fromString($invoice->id), StatusEnum::tryFrom($invoice->status), serialize($invoice));
            $approvalFacade = new ApprovalFacade();
            $approvalFacade->approve($approvalDto);

            return response([], 204)
                ->header('Content-Type', 'text/json');
        } catch (LogicException $ex) {
            return response(['error' => true, 'message' => $ex->getMessage()], 400)
                ->header('Content-Type', 'text/json');
        } catch (Exception $ex) {
            return response(['error' => true, 'message' => $ex->getMessage()], 400)
                ->header('Content-Type', 'text/json');
        }
    }

    /**
     * @param string $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function reject(string $id)
    {
        try {
            $invoiceService = new InvoiceService(DB::connection());
            $invoice = $invoiceService->getById($id);

            $approvalDto = new ApprovalDto(Uuid::fromString($invoice->id), StatusEnum::tryFrom($invoice->status), serialize($invoice));
            $approvalFacade = new ApprovalFacade();
            $approvalFacade->reject($approvalDto);

            return response([], 204)
                ->header('Content-Type', 'text/json');
        } catch (LogicException $ex) {
            return response(['error' => true, 'message' => $ex->getMessage()], 400)
                ->header('Content-Type', 'text/json');
        } catch (Exception $ex) {
            return response(['error' => true, 'message' => $ex->getMessage()], 400)
                ->header('Content-Type', 'text/json');
        }
    }
}
