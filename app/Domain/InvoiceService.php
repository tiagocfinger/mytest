<?php

namespace App\Domain;

use App\Domain\Enums\StatusEnum;
use Illuminate\Database\ConnectionInterface;

class InvoiceService
{
    /**
     * @param Connection $conn
     */
    public function __construct(private ConnectionInterface $conn)
    {
    }

    /**
     * @param string $number
     * @return mixed
     */
    public function getByNumber(string $number)
    {
        return $this->conn->table('invoices')
            ->where('number', $number)
            ->get()
            ->firstOrFail();
    }

    /**
     * @param string $id
     * @return mixed
     */
    public function getById(string $id)
    {
        return $this->conn->table('invoices')
            ->select(['invoices.*', 'companies.name AS company'])
            ->join('companies', 'companies.id', '=', 'invoices.company_id')
            ->where('invoices.id', $id)
            ->get()
            ->firstOrFail();
    }

    /**
     * @param string $id
     * @return int
     */
    public function approve(string $id)
    {
        return $this->conn->table('invoices')
            ->where('id', $id)
            ->update(['status' => StatusEnum::APPROVED]);
    }

    /**
     * @param string $id
     * @return int
     */
    public function reject(string $id)
    {
        return $this->conn->table('invoices')
            ->where('id', $id)
            ->update(['status' => StatusEnum::REJECTED]);
    }

    /**
     * @param string $id
     * @return void
     */
    public function getItens(string $id)
    {
        return $this->conn->table('invoices')
            ->select(['products.id', 'products.name', 'products.price', 'invoice_product_lines.quantity'])
            ->join('invoice_product_lines', 'invoice_product_lines.invoice_id', '=', 'invoices.id')
            ->join('products', 'products.id', '=', 'invoice_product_lines.product_id')
            ->where('invoices.id', $id)
            ->get();
    }
}
