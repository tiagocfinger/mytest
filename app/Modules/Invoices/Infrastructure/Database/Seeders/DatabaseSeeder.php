<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            ProductSeeder::class,
            CompanySeeder::class,
        ]);

        (new InvoiceSeeder(Db::connection()))->run();
    }
}
