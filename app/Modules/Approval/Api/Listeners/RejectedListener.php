<?php

namespace App\Modules\Approval\Api\Listeners;

use App\Domain\InvoiceService;
use App\Modules\Approval\Api\Events\EntityRejected;
use Illuminate\Support\Facades\DB;

class RejectedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\EntityRejected  $event
     * @return void
     */
    public function handle(EntityRejected $event)
    {
        $invoiceService = new InvoiceService(DB::connection());
        return $invoiceService->reject($event->approvalDto->id);
    }
}
