<?php

namespace App\Modules\Approval\Api\Listeners;

use App\Domain\InvoiceService;
use App\Modules\Approval\Api\Events\EntityApproved;
use Illuminate\Support\Facades\DB;

class ApprovedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * @param EntityApproved $event
     * @return int
     */
    public function handle(EntityApproved $event)
    {
        $invoiceService = new InvoiceService(DB::connection());
        return $invoiceService->approve($event->approvalDto->id);
    }
}
