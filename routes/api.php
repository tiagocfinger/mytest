<?php


use App\Infrastructure\Http\ApprovalController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/invoices/{id}', [ApprovalController::class, 'get']);
Route::patch('/invoices/{id}/approve', [ApprovalController::class, 'approve']);
Route::patch('/invoices/{id}/reject', [ApprovalController::class, 'reject']);
