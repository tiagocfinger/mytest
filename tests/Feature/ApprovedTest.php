<?php

namespace Tests\Feature;

use App\Domain\Enums\StatusEnum;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class ApprovedTest extends TestCase
{

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate', [
            '--env'  => 'testing',
            "--database" => "sqlite"
        ]);

        Artisan::call('db:seed');
    }

    /**
     * @return void
     */
    public function testInvoiceApproveFail()
    {
        $this->json('PATCH', 'api/invoices/0245d8a7-9e3b-4fb5-abfc-9f3076c35665/approve', [
            'Accept' => 'application/json'
        ])
            ->assertStatus(400)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * @return void
     */
    public function testInvoiceApproveSuccess()
    {
        $invoice = DB::table('invoices')
            ->get()
            ->firstOrFail();

        DB::table('invoices')
            ->where('id', $invoice->id)
            ->update(['status' => StatusEnum::DRAFT]);

        $this->json('PATCH', "api/invoices/{$invoice->id}/approve", [
            'Accept' => 'application/json'
        ])
            ->assertStatus(204);
    }

    /**
     * @return void
     */
    public function testInvoiceRejectFail()
    {
        $this->json('PATCH', 'api/invoices/0245d8a7-9e3b-4fb5-abfc-9f3076c35665/reject', [
            'Accept' => 'application/json'
        ])
            ->assertStatus(400)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * @return void
     */
    public function testInvoiceRejectSuccess()
    {
        $invoice = DB::table('invoices')
            ->get()
            ->firstOrFail();

        DB::table('invoices')
            ->where('id', $invoice->id)
            ->update(['status' => StatusEnum::DRAFT]);

        $this->json('PATCH', "api/invoices/{$invoice->id}/reject", [
            'Accept' => 'application/json'
        ])
            ->assertStatus(204);
    }

    // TESTE
    /**
     * @return void
     */
    public function testInvoiceGetFail()
    {
        $this->json('GET', 'api/invoices/0245d8a7-9e3b-4fb5-abfc-9f3076c35665', [
            'Accept' => 'application/json'
        ])
            ->assertStatus(400)
            ->assertJson([
                "error" => true,
            ]);
    }

    /**
     * @return void
     */
    public function testInvoiceGetSuccess()
    {
        $invoice = DB::table('invoices')
            ->get()
            ->firstOrFail();

        DB::table('invoices')
            ->where('id', $invoice->id)
            ->update(['status' => StatusEnum::DRAFT]);

        $this->json('GET', "api/invoices/{$invoice->id}", [
            'Accept' => 'application/json'
        ])
            ->assertStatus(200);
    }
}
